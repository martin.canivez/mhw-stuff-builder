use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::error;
use std::fs;

extern crate bincode;
extern crate rustc_serialize;

type Result<T> = std::result::Result<T, Box<dyn error::Error>>;

#[derive(Deserialize, Serialize, PartialEq)]
enum ArmorType {
    Head,
    Chest,
    Gloves,
    Waist,
    Legs,
}

#[derive(Deserialize, Serialize, PartialEq)]
struct DefenseObject {
    base: i32,
    max: i32,
    augmented: i32,
}

#[derive(Deserialize, Serialize, PartialEq)]
struct SkillRank {
    id: i32,
    skill_name: String,
    level: i8,
    modifiers: HashMap<String, i32>,
}

#[derive(Deserialize, Serialize, PartialEq)]
struct Slot {
    rank: i8,
}

#[derive(Deserialize, Serialize, PartialEq)]
struct Armor {
    id: i32,
    rank: String,
    name: String,
    r#type: ArmorType,
    defense: DefenseObject,
    skills: Vec<SkillRank>,
    slots: Vec<Slot>,
}

const ARMOR_LIST_CACHE_FILE_NAME: &str = "armorListCache.bin";

fn main() {
    let mut armor_list: Vec<Armor>;

    armor_list = match parse_file(ARMOR_LIST_CACHE_FILE_NAME) {
        Ok(list) => list,
        Err(e) => {
            println!("Can't get cache file, trying from web API : {}", e);
            let resp: Vec<Armor> = reqwest::blocking::get("https://mhw-db.com/armor")
                .expect("Can't get HTTP page")
                .json::<Vec<Armor>>()
                .expect("Can't get json");
            
            save_to_file(&resp, ARMOR_LIST_CACHE_FILE_NAME).unwrap_or_else(|e| println!("Error saving to file : {}", e));
            resp
        }
    };

    println!("size : {}", armor_list.len());

    armor_list.retain(|x| x.rank != "low");
    println!("Size after filter : {}", armor_list.len());
}

fn parse_file<T>(filename: &str) -> Result<T> where T: serde::de::DeserializeOwned {
    let armor_file = fs::File::open(filename)?;

    let armor_list: T = bincode::deserialize_from(&armor_file)?;

    Ok(armor_list)
}

fn save_to_file<T>(content: &T, filename: &str) -> Result<()> where T: serde::Serialize {
    let armor_file = fs::File::create(filename)?;

    bincode::serialize_into(&armor_file, content)?;

    Ok(())
}